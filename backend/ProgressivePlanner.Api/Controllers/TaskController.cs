﻿using Microsoft.AspNetCore.Mvc;
using ProgressivePlanner.Core.Dtos;
using ProgressivePlanner.Core.Entities;
using ProgressivePlanner.Core.Services;

namespace ProgressivePlanner.Api.Controllers;

[ApiController]
[Route("/api/task")]
public class TaskController : ControllerBase
{
    private readonly TaskService _taskService;

    public TaskController(TaskService taskService)
    {
        _taskService = taskService;
    }
    
    [HttpPost]
    public async Task<IActionResult> CreateTask(TaskDto dto)
    {
        await _taskService.CreateTask(dto);
        return Ok();
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<TaskItem>>> GetTasks()
    {
        return Ok(await _taskService.GetTaskItems());
    }
}