﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProgressivePlanner.Core.Abstractions;
using ProgressivePlanner.Infrastructure.DataAccess;

namespace ProgressivePlanner.Infrastructure;


public static class Module
{
    public static IServiceCollection RegisterInfrastructure(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("Default");
        services.AddDbContext<ApplicationDbContext>(opt => opt.UseNpgsql(connectionString));

        services.AddScoped<IDbContext, ApplicationDbContext>();
        return services;
    }
}