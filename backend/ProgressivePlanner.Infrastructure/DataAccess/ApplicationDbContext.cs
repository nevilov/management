﻿using Microsoft.EntityFrameworkCore;
using ProgressivePlanner.Core.Abstractions;
using ProgressivePlanner.Core.Entities;

namespace ProgressivePlanner.Infrastructure.DataAccess;

public class ApplicationDbContext : DbContext, IDbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Status>(opt =>
        {
            opt.HasKey(x => x.Name);
        });
        base.OnModelCreating(modelBuilder);
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
    {
        return base.SaveChangesAsync(cancellationToken);
    }

    public DbSet<TaskItem> TaskItems { get; set; }
    public DbSet<TaskLabel> TaskLabels { get; set; }
    public DbSet<Status> Statuses { get; set; }
    
}