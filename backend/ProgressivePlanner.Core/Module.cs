﻿using Microsoft.Extensions.DependencyInjection;
using ProgressivePlanner.Core.Services;

namespace ProgressivePlanner.Core;

public static class Module
{
    public static IServiceCollection AddApplicationContext(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<TaskService>();
        return serviceCollection;
    }
}