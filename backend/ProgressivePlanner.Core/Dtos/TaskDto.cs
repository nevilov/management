﻿using ProgressivePlanner.Core.Entities;

namespace ProgressivePlanner.Core.Dtos;

public class TaskDto
{
    public string Title { get; set; }
    public string Description { get; set; }
    public string Status { get; set; }
    public IEnumerable<string> Labels { get; set; }

    public TaskItem CreateTask() => new TaskItem()
    {
        Title = this.Title,
        CreatedAt = DateTime.UtcNow,
        Description = this.Description,
        TaskLabels = this.Labels.Select(x => new TaskLabel()
        {
            Name = x,
            CreatedAt = DateTime.UtcNow
        })
    };
}