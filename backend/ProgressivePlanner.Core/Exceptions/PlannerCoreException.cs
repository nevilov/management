﻿namespace ProgressivePlanner.Core.Exceptions;

public class PlannerCoreException : Exception
{
    public PlannerCoreException(string message): base(message)
    {

    }

}