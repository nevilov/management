﻿namespace ProgressivePlanner.Core.Entities;

public class Label
{
    public string Name { get; set; }
    public string Color { get; set; }
    
    public IEnumerable<TaskLabel> TaskLabels { get; set; }
}