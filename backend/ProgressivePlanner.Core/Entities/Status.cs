﻿namespace ProgressivePlanner.Core.Entities;

public class Status
{
    public string Name { get; set; }
    public string Color { get; set; }
}