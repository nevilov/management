﻿namespace ProgressivePlanner.Core.Entities;

public class TaskItem
{
    public Guid Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public DateTime? UpdatedAt { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime? DeletedAt { get; set; }
    public DateTime ScheduledAt { get; set; }
    public DayOfWeek DayOfWeek => ScheduledAt.DayOfWeek;
    public Status Status { get; set; }
    public IEnumerable<TaskLabel> TaskLabels { get; set; }
}