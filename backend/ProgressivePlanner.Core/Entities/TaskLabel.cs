﻿namespace ProgressivePlanner.Core.Entities;

public class TaskLabel
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime DeletedAt { get; set; }

}