﻿using Microsoft.EntityFrameworkCore;
using ProgressivePlanner.Core.Abstractions;
using ProgressivePlanner.Core.Dtos;
using ProgressivePlanner.Core.Entities;
using ProgressivePlanner.Core.Exceptions;

namespace ProgressivePlanner.Core.Services;

public class TaskService
{
    private readonly IDbContext _dbContext;

    public TaskService(IDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task CreateTask(TaskDto item)
    {
        var taskEntity = item.CreateTask();
        var status = _dbContext.Statuses.FirstOrDefault(x => x.Name == item.Status);
        if (status is null)
        {
            throw new PlannerCoreException("Status cannot be null");
        }
        taskEntity.Status = status;
        await _dbContext.TaskItems.AddAsync(taskEntity);
        await _dbContext.SaveChangesAsync();
    }

    public async Task<IEnumerable<TaskItem>> GetTaskItems()
    {
        return await _dbContext.TaskItems.ToListAsync();
    }
    
}