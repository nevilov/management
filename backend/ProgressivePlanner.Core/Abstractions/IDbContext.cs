﻿using Microsoft.EntityFrameworkCore;
using ProgressivePlanner.Core.Entities;

namespace ProgressivePlanner.Core.Abstractions;

public interface IDbContext
{
    DbSet<TaskItem> TaskItems { get; set; }
    DbSet<TaskLabel> TaskLabels { get; set; }
    DbSet<Status> Statuses { get; set; }
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken());
}