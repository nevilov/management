﻿'use client'
import React, { useState } from 'react';
import Grid from '@mui/material/Grid';
import {
    useSensors,
    useSensor,
    PointerSensor,
    KeyboardSensor,
    DndContext,
    closestCorners,
    DragEndEvent,
    DragStartEvent,
    DragOverEvent,
    DragOverlay,
    DropAnimation,
    defaultDropAnimation,
} from '@dnd-kit/core';
import { sortableKeyboardCoordinates, arrayMove } from '@dnd-kit/sortable';
import {findBoardSectionContainer, initializeBoard} from "@/core/utils/board";
import BoardSection from "@/components/dnd/verticalList/verticalList";
import TaskItem from "@/components/dnd/taskCard/taskCard";
import {getTaskById} from "@/core/utils/util";
import {INITIAL_TASKS} from "@/data/tasks";
import {BoardSections} from "@/core/types/management/TaskItem";
import ManagementStyled from "@/app/management/page.styled.";

const BoardSectionList = () => {
    const tasks = INITIAL_TASKS;
    const initialBoardSections = initializeBoard(INITIAL_TASKS);
    const [boardSections, setBoardSections] =
        useState<BoardSections>(initialBoardSections);

    const [activeTaskId, setActiveTaskId] = useState<null | string>(null);

    const sensors = useSensors(
        useSensor(PointerSensor, {
            activationConstraint: {
                distance: 8,
            }
        }),
        useSensor(KeyboardSensor, {
            coordinateGetter: sortableKeyboardCoordinates,
        })
    );

    const handleDragStart = ({ active }: DragStartEvent) => {
        setActiveTaskId(active.id as string);
    };

    const handleDragOver = ({ active, over }: DragOverEvent) => {
        // Find the containers
        const activeContainer = findBoardSectionContainer(
            boardSections,
            active.id as string
        );
        const overContainer = findBoardSectionContainer(
            boardSections,
            over?.id as string
        );

        if (
            !activeContainer ||
            !overContainer ||
            activeContainer === overContainer
        ) {
            return;
        }

        setBoardSections((boardSection) => {
            console.log(boardSection)
            const activeItems = boardSection[activeContainer];
            const overItems = boardSection[overContainer];

            // Find the indexes for the items
            const activeIndex = activeItems.findIndex(
                (item) => item.id === active.id
            );
            const overIndex = overItems.findIndex((item) => item.id !== over?.id);

            return {
                ...boardSection,
                [activeContainer]: [
                    ...boardSection[activeContainer].filter(
                        (item) => item.id !== active.id
                    ),
                ],
                [overContainer]: [
                    ...boardSection[overContainer].slice(0, overIndex),
                    boardSections[activeContainer][activeIndex],
                    ...boardSection[overContainer].slice(
                        overIndex,
                        boardSection[overContainer].length
                    ),
                ],
            };
        });
    };

    const handleDragEnd = ({ active, over }: DragEndEvent) => {
        const activeContainer = findBoardSectionContainer(
            boardSections,
            active.id as string
        );
        const overContainer = findBoardSectionContainer(
            boardSections,
            over?.id as string
        );

        if (
            !activeContainer ||
            !overContainer ||
            activeContainer !== overContainer
        ) {
            return;
        }

        const activeIndex = boardSections[activeContainer].findIndex(
            (task) => task.id === active.id
        );
        const overIndex = boardSections[overContainer].findIndex(
            (task) => task.id === over?.id
        );

        if (activeIndex !== overIndex) {
            setBoardSections((boardSection) => ({
                ...boardSection,
                [overContainer]: arrayMove(
                    boardSection[overContainer],
                    activeIndex,
                    overIndex
                ),
            }));
        }

        setActiveTaskId(null);
    };

    const dropAnimation: DropAnimation = {
        ...defaultDropAnimation,
    };

    const task = activeTaskId ? getTaskById(tasks, activeTaskId) : null;

    return (
        <ManagementStyled>
            <DndContext
                sensors={sensors}
                collisionDetection={closestCorners}
                onDragStart={handleDragStart}
                onDragOver={handleDragOver}
                onDragEnd={handleDragEnd}
            >
                <Grid container spacing={4}>
                    {Object.keys(boardSections).map((boardSectionKey) => (
                        <Grid item xs={4} key={boardSectionKey}>
                            <BoardSection
                                id={boardSectionKey}
                                title={boardSectionKey}
                                tasks={boardSections[boardSectionKey]}
                            />
                        </Grid>
                    ))}
                    <DragOverlay dropAnimation={dropAnimation}>
                        {task ? <TaskItem task={task} /> : null}
                    </DragOverlay>
                </Grid>
            </DndContext>
        </ManagementStyled>
    );
};

export default BoardSectionList;
