﻿import Link from "next/link";
export const header = () => {
    
    return (
            <header>
                <div>
                    ProgressivePlanner
                </div>

                <menu>
                    <div className="item">
                        <Link href={"/management"}>Planner</Link>
                    </div>
                    <div className="item">
                        <Link href={"/notes"}>Notes</Link>
                    </div>
                </menu>
            </header>
    )
}

export default header;