﻿import { Card, CardContent } from '@mui/material';
import {TaskItem} from "@/core/types/management/TaskItem";
import {Modal} from "@mui/base";
import {useState} from "react";

type TaskItemProps = {
    task: TaskItem;
};

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const TaskItem = ({ task }: TaskItemProps) => {
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    
    return (
        <div>                    
            <Card onClick={() => handleOpen()}>
                <CardContent>{task.title}</CardContent>
            </Card>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="keep-mounted-modal-title"
                aria-describedby="keep-mounted-modal-description"
            >
                <div className="">
                    {task.title}
                    
                </div>
            </Modal>
        </div>
    );
};

export default TaskItem;
