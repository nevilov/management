﻿import React from 'react';
import Box from '@mui/material/Box';
import { useDroppable } from '@dnd-kit/core';
import {
    SortableContext,
    verticalListSortingStrategy,
} from '@dnd-kit/sortable';
import Typography from '@mui/material/Typography';
import SortableTaskItem from "@/components/dnd/sortableTaskItem/sortableTaskItem";
import TaskItem from "@/components/dnd/taskCard/taskCard";

type BoardSectionProps = {
    id: string;
    title: string;
    tasks: TaskItem[];
};

const BoardSection = ({ id, title, tasks }: BoardSectionProps) => {
    const { setNodeRef } = useDroppable({
        id,
    });

    return (
        <Box sx={{ backgroundColor: '#727272', padding: 2 }}>
            <Typography variant="h6" sx={{ mb: 2 }}>
                {title}
            </Typography>
            <SortableContext
                id={id}
                items={tasks}
                strategy={verticalListSortingStrategy}
            >
                <div ref={setNodeRef}>
                    {tasks.map((task) => (
                        <div onClick={() => console.log('sds')}>
                            <Box key={task.id} sx={{ mb: 2 }}>
                                    <SortableTaskItem id={task.id}>
                                        <TaskItem task={task} />
                                    </SortableTaskItem>
                            </Box>
                        </div>
                            
                    ))}
                </div>
            </SortableContext>
        </Box>
    );
};

export default BoardSection;
