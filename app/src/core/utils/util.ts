﻿import {Label, TaskItem} from "@/core/types/management/TaskItem";

export const getTasksByStatus = (tasks: TaskItem[], status: Label) => {
    return tasks.filter((task) => task.status === status);
};

export const getTaskById = (tasks: TaskItem[], id: string) => {
    return tasks.find((task) => task.id === id);
};
