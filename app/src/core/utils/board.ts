﻿import {BoardSections, Label, TaskItem} from "@/core/types/management/TaskItem";
import {getTasksByStatus} from "@/core/utils/util";
import {BOARD_SECTIONS} from "@/data/tasks";

export const initializeBoard = (tasks: TaskItem[]) => {
    const boardSections: BoardSections = {};

    Object.keys(BOARD_SECTIONS).forEach((boardSectionKey) => {
        boardSections[boardSectionKey] = getTasksByStatus(
            tasks,
            boardSectionKey as Label
        );
    });

    return boardSections;
};

export const findBoardSectionContainer = (
    boardSections: BoardSections,
    id: string
) => {
    if (id in boardSections) {
        return id;
    }

    const container = Object.keys(boardSections).find((key) =>
        boardSections[key].find((item) => item.id === id)
    );
    return container;
};
