﻿export type Label = 'monday' | 'tuesday' | 'wednesday' | 'thursday' | 'friday' | 'saturday';

export type TaskItem = {
    id: string;
    title: string;
    description: string;
    created_date: Date,
    status: Label;
};

export type BoardSections = {
    [name: string]: TaskItem[];
};
