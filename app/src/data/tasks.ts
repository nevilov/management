﻿import { v4 as uuidv4 } from 'uuid';
import {TaskItem} from "@/core/types/management/TaskItem";

export const INITIAL_TASKS: TaskItem[] = [
    {
        id: uuidv4(),
        title: 'Title 2',
        description: 'Desc 2',
        created_date: new Date,
        status: 'saturday',
    },
    {
        id: uuidv4(),
        title: 'Title 3',
        description: 'Desc 3',
        created_date: new Date,
        status: 'monday',
    },
    {
        id: uuidv4(),
        title: 'Title 4',
        description: 'Desc 4',
        created_date: new Date,
        status: 'thursday',
    },
];

export const BOARD_SECTIONS = {
    monday: 'monday',
    tuesday: '',
    wednesday: '',
    thursday: '',
    friday: '',
    saturday: ''
};
